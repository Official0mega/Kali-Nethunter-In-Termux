# Kali-Nethunter-In-Termux
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fraynerchuah%2FKali-Nethunter-In-Termux.svg?type=shield)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fraynerchuah%2FKali-Nethunter-In-Termux?ref=badge_shield)

This is a script by which you can install Kali Nethunter (Kali Linux) in your termux application without rooted phone.
### Steps For Installation
### Install Kali Nethunter Full Version
1. Download script in **HOME** `wget https://gitlab.com/rc-chuah/Kali-Nethunter-In-Termux/raw/master/install-nethunter-full-termux`
2. Give execution permission `chmod +x install-nethunter-full-termux`
3. Run script `./install-nethunter-full-termux`
4. Now just start kali nethunter `nethunter`

### Install Kali Nethunter Minimal Version
1. Download script in **HOME** `wget https://gitlab.com/rc-chuah/Kali-Nethunter-In-Termux/raw/master/install-nethunter-minimal-termux`
2. Give execution permission `chmod +x install-nethunter-minimal-termux`
3. Run script `./install-nethunter-minimal-termux`
4. Now just start kali nethunter `nethunter`

#### Credited To Offensive Security

## License
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fraynerchuah%2FKali-Nethunter-In-Termux.svg?type=large)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fraynerchuah%2FKali-Nethunter-In-Termux?ref=badge_large)

#### You have any idea to improve ? So Just give PR
